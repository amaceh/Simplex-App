# SIMPLEX METHOD APPS
a windows application to implement well-known simplex method to solve linear programming problem using c# to run in windows platform

Currently this project is private, but might be go public in january

## what we will create
- basic simplex implementation
- BIG-M simplex implementation

## changelog
24-des-2017
* error handling
* value input with on cell listview
* highlight important row and column 
* Basic Maximation Simplex Done

25-des-2017
* input behavior fix
* constraint number in simplex fixed

28-des-2017
* add minimization problem
* checking bug, found and fixed

30-des-2017
* big-M is running as far as i test it, but the M is still show its number (1x10^6 ) instead of its variable
* floating point use "," not "."

03-jan-2018
* data saved, back button work
* add little misc to UI

04-jan-2018
* windows UI change
* now add some icon