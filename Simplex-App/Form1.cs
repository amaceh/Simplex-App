﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simplex_App
{
    public partial class Form1 : Form
    {
        public static string method { get; set; }
        public Form1()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProblemInputForm next = new ProblemInputForm();
            method = "basic";
            next.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProblemInputForm next = new ProblemInputForm();
            method = "big-M";
            next.Show();
            this.Hide();
        }
    }
}
