﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simplex_App
{
    public partial class ConstraintInputForm : Form
    {

        private readonly TextBox txt = new TextBox { BorderStyle = BorderStyle.FixedSingle, Visible = false };
        private readonly TextBox txt2 = new TextBox { BorderStyle = BorderStyle.FixedSingle, Visible = false };

        public static ListView dataZ { get; set; }
        public static ListView dataC { get; set; }
        public static int artNum { get; set; }
        public static int slackNum { get; set; }

        private string previousInput = "";

        int x = 0, y = 0;
        public ConstraintInputForm()
        {
            InitializeComponent();
            //to add edit textbox to listview
            LVInput.FullRowSelect = true;
            lvZ.FullRowSelect = true;

            LVInput.Controls.Add(txt);
            lvZ.Controls.Add(txt2);
            txt.ShortcutsEnabled = false;
            txt2.ShortcutsEnabled = false;
            txt.TextChanged += (o, e) => textChanged(txt);
            txt2.TextChanged += (o, e) => textChanged(txt2);

            txt.Leave += (o, e) => updateSubItem();
            txt2.Leave += (o, e) => updateSubItemZ();


            LVInput.Columns.Add("No");
            /* Header Creation */
            for (int i = 0; i < ProblemInputForm.varNum; i += 1)
            {
                LVInput.Columns.Add("x" + (i + 1));
            }
            LVInput.Columns.Add("Tanda");
            LVInput.Columns.Add("Nilai");

            /* ROW Creation */
            for (int i = 0; i < ProblemInputForm.conNum; i += 1)
            {
                ListViewItem item = new ListViewItem("" + (i + 1));
                for (int j = 0; j < ProblemInputForm.varNum; j += 1)
                {
                    item.SubItems.Add("0");
                }
                if (ProblemInputForm.kind == "Maximize")
                {
                    item.SubItems.Add("<=");
                }
                else if (ProblemInputForm.kind == "Minimize")
                {
                    item.SubItems.Add(">=");
                }
                item.SubItems.Add("0");
                LVInput.Items.Add(item);
            }

            /* Z Creation */
            lvZ.Columns.Add("No");
            /* Header Creation */
            for (int i = 0; i < ProblemInputForm.varNum; i += 1)
            {
                lvZ.Columns.Add("x" + (i + 1));
            }
            //lvZ.Columns.Add("Tanda");
            //lvZ.Columns.Add("Nilai");

            /* ROW Creation */
            ListViewItem item2 = new ListViewItem("Z");
            for (int j = 0; j < ProblemInputForm.varNum; j += 1)
            {
                item2.SubItems.Add("0");
            }
            //item2.SubItems.Add("=");
            //item2.SubItems.Add("0");
            lvZ.Items.Add(item2);
        }

        private void Txt_TextChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized) {
                this.WindowState = FormWindowState.Normal;
            } else
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void LVInput_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo hit = LVInput.HitTest(e.Location);
            x = Convert.ToInt32(hit.Item.Index);
            y = Convert.ToInt32(hit.Item.SubItems.IndexOf(hit.SubItem));
            if (y != 0)
            {
                if ((Form1.method == "basic" && y != ProblemInputForm.varNum + 1)
                    || Form1.method == "big-M")//larang edit tanda untuk simplex basic
                {
                    Rectangle rowBounds = hit.SubItem.Bounds;
                    Rectangle labelBounds = hit.Item.GetBounds(ItemBoundsPortion.Label);
                    int leftMargin = labelBounds.Left - 1;
                    txt.Bounds = new Rectangle(rowBounds.Left + leftMargin, rowBounds.Top, rowBounds.Width - leftMargin - 1, rowBounds.Height);
                    txt.Text = hit.SubItem.Text;
                    txt.SelectAll();
                    txt.Visible = true;
                    txt.Focus();
                }
            }
        }

        private void lvZ_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo hit = lvZ.HitTest(e.Location);
            x = Convert.ToInt32(hit.Item.Index);
            y = Convert.ToInt32(hit.Item.SubItems.IndexOf(hit.SubItem));
            if (y != 0)
            {
                Rectangle rowBounds = hit.SubItem.Bounds;
                Rectangle labelBounds = hit.Item.GetBounds(ItemBoundsPortion.Label);
                int leftMargin = labelBounds.Left - 1;
                txt2.Bounds = new Rectangle(rowBounds.Left + leftMargin, rowBounds.Top, rowBounds.Width - leftMargin - 1, rowBounds.Height);
                txt2.Text = hit.SubItem.Text;
                txt2.SelectAll();
                txt2.Visible = true;
                txt2.Focus();
            }
        }

        private void updateSubItem()
        {
            txt.Visible = false;
            if (!string.IsNullOrWhiteSpace(txt.Text) && (((y != ProblemInputForm.varNum + 1) && txt.Text != "-") ||
                ((y == ProblemInputForm.varNum + 1) && (txt.Text == "<=" || txt.Text == "=" || txt.Text == ">="))))
            {
                ListViewItem item = LVInput.Items[x];
                item.SubItems[y].Text = txt.Text;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProblemInputForm back = new ProblemInputForm();
            back.setTextBox();
            back.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dataZ = lvZ;
            dataC = LVInput;
            if (ProblemInputForm.kind == "Minimize" && Form1.method == "basic")
            {
                MessageBox.Show("Dalam kasus Minimasi, data akan mengalami Transpose", "Informasi",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (Form1.method == "big-M")
            {
                countSlackAndArtificial();
            }

            SimplexProgressForm go = new SimplexProgressForm();
            go.Show();
            this.Hide();
        }

        private void updateSubItemZ()
        {
            txt2.Visible = false;

            if (!string.IsNullOrWhiteSpace(txt2.Text) && txt2.Text != "-")
            {
                ListViewItem item = lvZ.Items[x];
                item.SubItems[y].Text = txt2.Text;
            }
        }

        //Regex regex = new Regex("[^-]+[^0-9]+");
        //e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        //e.Handled = regex.IsMatch(e.KeyChar);
        private void textChanged(TextBox text)
        {
            Regex r = new Regex(@"^-{0,1}\d+\,{0,1}\d*$"); // This is the main part, can be altered to match any desired form or limitations
            Regex t = new Regex(@"^(=|>|<|>=|<|<=|<>)$");
            Match m = r.Match(text.Text);
            Match m2 = t.Match(text.Text);
            //allow to be blank while edit
            if ((m.Success || string.IsNullOrWhiteSpace(text.Text) || text.Text == "-") ||
                ((y == ProblemInputForm.varNum + 1) && (m2.Success || string.IsNullOrWhiteSpace(text.Text))))
            {
                previousInput = text.Text;
            }
            else
            {
                text.Text = previousInput;
            }
        }

        private void ConstraintInputForm_Load(object sender, EventArgs e)
        {
            if (Form1.method == "basic")
            {
                lbMethod.Text = "Simplex Basic";
            }
            else if (Form1.method == "big-M")
            {
                lbMethod.Text = "Simplex Penalty";
            }

            if (ProblemInputForm.kind == "Maximize")
            {
                lbMethod.Text += " > Maximize";
            }
            else if (ProblemInputForm.kind == "Minimize")
            {
                lbMethod.Text += " > Minimize";
            }
        }

        private void countSlackAndArtificial()
        {
            slackNum = 0;
            artNum = 0;
            foreach (ListViewItem item in dataC.Items)
            {
                if (item.SubItems[ProblemInputForm.varNum+1].Text == "<=")
                {
                    slackNum+=1;
                }else if (item.SubItems[ProblemInputForm.varNum + 1].Text == "=")
                {
                    artNum += 1;
                }
                else if (item.SubItems[ProblemInputForm.varNum + 1].Text == ">=")
                {
                    slackNum += 1;
                    artNum += 1;
                }
            }
        }

        private void ConstraintInputForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();

        }

        public void reloads()
        {
            lvZ.Items.Clear();
            lvZ.Items.AddRange((from ListViewItem item in dataZ.Items
                                      select (ListViewItem)item.Clone()
                  ).ToArray());
            LVInput.Items.Clear();
            LVInput.Items.AddRange((from ListViewItem item in dataC.Items
                                select (ListViewItem)item.Clone()
                  ).ToArray());

            //lvZ = dataZ;
            //LVInput = dataC;
        }
    }
}
