﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simplex_App
{
    public partial class ProblemInputForm : Form
    {
        public static int conNum { get; set; }
        public static int varNum { get; set; }
        public static string kind { get; set; }
        
        public ProblemInputForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedDialog;

            //wise man said never trust user
            tbCon.ShortcutsEnabled = false;
            tbVar.ShortcutsEnabled = false;

            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(tbVar.Text) &&
                !string.IsNullOrWhiteSpace(tbCon.Text) &&
                !string.IsNullOrWhiteSpace(cbKind.Text))
            {
                conNum = Convert.ToInt32(tbCon.Text);
                varNum = Convert.ToInt32(tbVar.Text);
                kind = cbKind.Text;
                
                 //TextBox Accept Number only, not even negative sign
                if (conNum<1 || varNum <1)
                {
                    MessageBox.Show("Jumlah tidak boleh Nol", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                /*
                else if (varNum>conNum)
                {
                    MessageBox.Show("Jumlah Variabel Terlalu Banyak", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                
                 It Is Ok to have so much constraint?
                else if (varNum > conNum)
                {
                    MessageBox.Show("Jumlah Hambatan Terlalu Banyak", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                */
            }
            else
            {
                MessageBox.Show("Masukkan Data dengan benar", "Error", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ConstraintInputForm next = new ConstraintInputForm();
            next.Show();
            this.Hide();
        }


        //wise man said never trust user
        private void tbVar_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void tbCon_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 back = new Form1();
            back.Show();
            this.Hide();
        }

        private void tbVar_TextChanged(object sender, EventArgs e)
        {
            tbCon.Text = tbVar.Text;
        }

        private void ProblemInputForm_Load(object sender, EventArgs e)
        {
            if (Form1.method == "basic")
            {
                lbMethod.Text = "Simplex Basic";
            }
            else if (Form1.method == "big-M")
            {
                lbMethod.Text = "Simplex Penalty";
            }
        }

        public void setTextBox()
        {
            cbKind.Text = kind;
            tbVar.Text = varNum + "";
            tbCon.Text = conNum + "";
        }

        private void ProblemInputForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
