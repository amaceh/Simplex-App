﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simplex_App
{
    public partial class SimplexProgressForm : Form
    {
        private List<ListView> iterationData = new List<ListView>();
        private int MOST_COLUMN;
        private int MOST_ROW;
        private bool IS_COMPLETE = false;
        private readonly double M = 1000000;
        private bool M_IS_OK = false;

        private int iterasi = 0;
        private int phase = 0;
        public SimplexProgressForm()
        {
            InitializeComponent();
            lbIterasi.Text=""+iterasi;
            initializeItem();
            if (Form1.method == "basic")
            {
                chooseRow();
                chooseColumn();
            }
            /*
            else if (Form1.method == "big-M")
            {
                initializeM();
                //chooseRow();
                //chooseColumn();
            }
            */
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        /*
         *
         * Here The Next Button
         * 
         */
        private void button2_Click(object sender, EventArgs e)
        {
            if (!IS_COMPLETE)
            {
                phase += 1;
                ListView temp = new ListView();
                temp.Items.AddRange((from ListViewItem item in lvSimplex.Items
                                          select (ListViewItem)item.Clone()
                      ).ToArray());
                iterationData.Add(temp);
                if (Form1.method=="big-M" && !M_IS_OK)
                {
                    initializeM();
                    if (M_IS_OK)
                    {
                        chooseRow();
                        chooseColumn();
                    }
                }
                else
                {
                    iterasi += 1;
                    lbIterasi.Text = "" + iterasi;
                    removeHighLightColumn();
                    removeHighLightRow();
                    rowOperationBasis();
                    rowOperationNonBasis();
                    chooseRow();
                    chooseColumn();
                    if (IS_COMPLETE)
                    {
                        clearRatio();
                        highLightZ();
                        button2.Text = "Selesai";
                    }
                }
            }
            else
            {
                /*
                clearRatio();
                highLightZ();
                button2.Text = "Selesai";
                */
            }
        }

        private void chooseRow()
        {
            IS_COMPLETE = true;
            double a = 9999999999;
            int limit=0;
            if (Form1.method=="basic")
                limit = ProblemInputForm.varNum + ProblemInputForm.conNum;
            else if (Form1.method == "big-M")
            {
                limit = ProblemInputForm.varNum + ConstraintInputForm.artNum + ConstraintInputForm.slackNum;
                if (ProblemInputForm.kind == "Minimize")
                    a *= -1;
            }


            for (int i = 0; i < limit; i += 1)
            {

                double num = Convert.ToDouble(lvSimplex.Items[0].SubItems[i + 2].Text);
                if (Form1.method == "big-M" && ProblemInputForm.kind == "Minimize")
                {
                    if (num > a && num > 0)
                    {


                        MOST_COLUMN = i + 2;
                        a = num;
                        IS_COMPLETE = false;
                    }
                }
                else
                {
                    if (num < a && num < 0)
                    {
                    
                    
                        MOST_COLUMN = i + 2;
                        a = num;
                        IS_COMPLETE = false;
                    }
                }
            }
            
            if (!IS_COMPLETE)
            {
                highLightColumn();
            }
        }

        private void chooseColumn()
        {
            double a = 9999999999;
            if (!IS_COMPLETE)
            {
                for (int i = 0; i < ProblemInputForm.conNum; i += 1)
                {
                    double num = Convert.ToDouble(lvSimplex.Items[i + 1].SubItems[MOST_COLUMN].Text);
                    double num2=0;
                    if (Form1.method == "basic")
                        num2 = Convert.ToDouble(lvSimplex.Items[i + 1].SubItems[(ProblemInputForm.conNum+ProblemInputForm.varNum) + 2].Text);
                    else if (Form1.method == "big-M")
                        num2 = Convert.ToDouble(lvSimplex.Items[i + 1].SubItems[(ConstraintInputForm.artNum + ConstraintInputForm.slackNum + ProblemInputForm.varNum) + 2].Text);

                    //if (num != 0 && num2 != 0)
                    //{
                    //C# is compact enough to handle 0/x and even 0/0
                    double ratio = num2 / num;
                    if(Form1.method=="basic")
                        lvSimplex.Items[i + 1].SubItems[(ProblemInputForm.conNum + ProblemInputForm.varNum) + 3].Text = ratio + "";
                    else if (Form1.method == "big-M")
                        lvSimplex.Items[i + 1].SubItems[(ConstraintInputForm.artNum + ConstraintInputForm.slackNum + ProblemInputForm.varNum) + 3].Text = ratio + "";

                    if (ratio>0 && ratio < a)
                        {
                            a = ratio;
                            MOST_ROW = i + 1;
                        }
                    //}
                }
                highLightRow();
            }
        }

        private void rowOperationBasis()
        {
            double point = Convert.ToDouble(lvSimplex.Items[MOST_ROW].SubItems[MOST_COLUMN].Text);
            bool first = true;
            if (point > 0 && point != 1)
            {
                foreach (ListViewItem.ListViewSubItem item in lvSimplex.Items[MOST_ROW].SubItems)
                {
                    if (!first)
                    {
                        double beforeOp = Convert.ToDouble(item.Text);
                        item.Text = "" + (beforeOp / point);
                    }
                    else
                    {
                        item.Text = lvSimplex.Columns[MOST_ROW + 1].Text;
                    }
                    first = false;
                }
            }
        }

        private void rowOperationNonBasis()
        {
            for (int i=0; i< lvSimplex.Items.Count; i += 1)
            {
                if (i != MOST_ROW)
                {
                    double point = Convert.ToDouble(lvSimplex.Items[i].SubItems[MOST_COLUMN].Text) 
                        / Convert.ToDouble(lvSimplex.Items[MOST_ROW].SubItems[MOST_COLUMN].Text);
                    for (int j = 2; j < lvSimplex.Items[i].SubItems.Count; j += 1)
                    {
                        if (lvSimplex.Items[i].SubItems[j].Text != "")
                        {
                            double beforeOp = Convert.ToDouble(lvSimplex.Items[i].SubItems[j].Text);
                            double diffOp = Convert.ToDouble(lvSimplex.Items[MOST_ROW].SubItems[j].Text) * point;
                            double result = beforeOp - diffOp;
                            lvSimplex.Items[i].SubItems[j].Text = "" + result;
                        }
                    }
                }
            }
        }



        private void highLightRow()
        {
            foreach (ListViewItem.ListViewSubItem item in lvSimplex.Items[MOST_ROW].SubItems)
            {
                item.BackColor = Color.LightBlue;
            }
        }

        private void highLightColumn()
        {
            for (int i = 0; i < ProblemInputForm.conNum + 1; i += 1)
            {
                lvSimplex.Items[i].UseItemStyleForSubItems = false;
                lvSimplex.Items[i].SubItems[MOST_COLUMN].BackColor = Color.LightBlue;
            }
        }

        private void removeHighLightRow()
        {
            foreach (ListViewItem.ListViewSubItem item in lvSimplex.Items[MOST_ROW].SubItems)
            {
                item.BackColor = Color.White;
            }
        }

        private void removeHighLightColumn()
        {
            for (int i = 0; i < ProblemInputForm.conNum + 1; i += 1)
            {
                lvSimplex.Items[i].UseItemStyleForSubItems = false;
                lvSimplex.Items[i].SubItems[MOST_COLUMN].BackColor = Color.White;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //switched because of transpose
            if (ProblemInputForm.kind == "Minimize" && Form1.method == "basic")
            {
                int temp = ProblemInputForm.varNum;
                ProblemInputForm.varNum = ProblemInputForm.conNum;
                ProblemInputForm.conNum = temp;
            }
            ConstraintInputForm back = new ConstraintInputForm();
            back.reloads();
            back.Show();
            this.Hide();
        }

        private void clearRatio()
        {
            for (int i = 0; i < ProblemInputForm.conNum; i += 1)
            {
                if (Form1.method == "basic")
                    lvSimplex.Items[i + 1].SubItems[(ProblemInputForm.varNum + ProblemInputForm.conNum) + 3].Text = "";
                else if (Form1.method == "big-M")
                    lvSimplex.Items[i + 1].SubItems[(ProblemInputForm.varNum + ConstraintInputForm.slackNum+ ConstraintInputForm.artNum) + 3].Text = "";


            }
        }

        private void highLightZ()
        {
            foreach (ListViewItem.ListViewSubItem item in lvSimplex.Items[0].SubItems)
            {
                item.BackColor = Color.Yellow;
            }
        }

        private void removeHighLightZ()
        {
            foreach (ListViewItem.ListViewSubItem item in lvSimplex.Items[0].SubItems)
            {
                item.BackColor = Color.White;
            }
        }

        private void initializeItem()
        {
            if (Form1.method=="basic")
            {
                if (ProblemInputForm.kind == "Minimize")
                {
                    //switched because of transpose
                    int temp = ProblemInputForm.varNum;
                    ProblemInputForm.varNum = ProblemInputForm.conNum;
                    ProblemInputForm.conNum = temp;
                }
                lvSimplex.Columns.Add("Basis");
                lvSimplex.Columns.Add("Z");
                
                for (int i = 0; i < ProblemInputForm.varNum; i += 1)
                {
                    lvSimplex.Columns.Add("x" + (i + 1));
                }
                for (int i = 0; i < ProblemInputForm.conNum; i += 1)
                {
                    lvSimplex.Columns.Add("s" + (i + 1));
                }
                lvSimplex.Columns.Add("Nilai");
                lvSimplex.Columns.Add("Rasio");

                ListViewItem item;
                ListViewItem data = ConstraintInputForm.dataZ.Items[0];

                if (ProblemInputForm.kind == "Maximize")
                {
                    item = new ListViewItem("Z");
                    item.SubItems.Add("1");
                    for (int j = 0; j < ProblemInputForm.varNum; j += 1)
                    {
                        double x = Convert.ToDouble(data.SubItems[j + 1].Text);
                        if (x > 0)
                            item.SubItems.Add("-" + x);
                        else
                        {
                            x = x * -1;
                            item.SubItems.Add(x + "");
                        }
                    }
                    for (int j = 0; j < ProblemInputForm.conNum; j += 1)
                    {
                        item.SubItems.Add("0");
                    }
                    item.SubItems.Add("0");
                    item.SubItems.Add("");
                    lvSimplex.Items.Add(item);
                    for (int i = 0; i < ProblemInputForm.conNum; i += 1)
                    {
                        item = new ListViewItem("s" + (i + 1));
                        item.SubItems.Add("0");
                        data = ConstraintInputForm.dataC.Items[i];
                        for (int j = 0; j < ProblemInputForm.varNum; j += 1)
                        {
                            item.SubItems.Add(data.SubItems[j + 1].Text);
                        }
                        for (int j = 0; j < ProblemInputForm.conNum; j += 1)
                        {
                            if (i == j)
                            {
                                item.SubItems.Add("1");
                            }
                            else
                            {
                                item.SubItems.Add("0");
                            }
                        }
                        item.SubItems.Add(data.SubItems[ProblemInputForm.varNum + 2]);
                        item.SubItems.Add("");
                        lvSimplex.Items.Add(item);
                    }
                }else if (ProblemInputForm.kind == "Minimize")
                {
                    //TO DO remember that column and row Number Is Swithed!!
                    //standard minimization require transpose, so we should doin a transpose first!

                    List<List<string>> result = new List<List<string>>();

                    //ProblemInputForm.varNum        
                    for (int i=0; i < ProblemInputForm.conNum; i+=1)
                    {
                        List<string> row = new List<string>();
                        for (int j = 0; j < ProblemInputForm.varNum; j+=1)
                        {
                            row.Add(ConstraintInputForm.dataC.Items[j].SubItems[i+1].Text);
                        }
                        //if (i < ProblemInputForm.conNum - 1)
                            row.Add(ConstraintInputForm.dataZ.Items[0].SubItems[i + 1].Text);
                        //else
                        //    row.Add("0");
                        result.Add(row);
                    }
                    List<string> row2 = new List<string>();
                    for (int j = 0; j < ProblemInputForm.varNum; j += 1)
                    {
                        row2.Add(ConstraintInputForm.dataC.Items[j].SubItems[ProblemInputForm.conNum + 2].Text);
                    }
                    row2.Add("0");
                    result.Add(row2);
                    //result = result;

                    item = new ListViewItem("Z");
                    item.SubItems.Add("1");
                    for (int j = 0; j < ProblemInputForm.varNum; j += 1)
                    {
                        double x = Convert.ToDouble(result[ProblemInputForm.conNum][j]);
                        if (x > 0)
                            item.SubItems.Add("-" + x);
                        else
                        {
                            x = x * -1;
                            item.SubItems.Add(x + "");
                        }
                    }
                    for (int j = 0; j < ProblemInputForm.conNum; j += 1)
                    {
                        item.SubItems.Add("0");
                    }
                    item.SubItems.Add("0");
                    item.SubItems.Add("");
                    lvSimplex.Items.Add(item);
                    for (int i = 0; i < ProblemInputForm.conNum; i += 1)
                    {
                        item = new ListViewItem("s" + (i + 1));
                        item.SubItems.Add("0");
                        //data = ConstraintInputForm.dataC.Items[i];
                        for (int j = 0; j < ProblemInputForm.varNum; j += 1)
                        {
                            
                            item.SubItems.Add(result[i][j]);
                        }
                        for (int j = 0; j < ProblemInputForm.conNum; j += 1)
                        {
                            if (i == j)
                            {
                                item.SubItems.Add("1");
                            }
                            else
                            {
                                item.SubItems.Add("0");
                            }
                        }
                        item.SubItems.Add(result[i][ProblemInputForm.varNum]);
                        item.SubItems.Add("");
                        lvSimplex.Items.Add(item);
                    }
                }
            }else if (Form1.method=="big-M")
            {
                lvSimplex.Columns.Add("Basis");
                lvSimplex.Columns.Add("Z");

                for (int i = 0; i < ProblemInputForm.varNum; i += 1)
                {
                    lvSimplex.Columns.Add("x" + (i + 1));
                }
                for (int i = 0; i < ConstraintInputForm.slackNum; i += 1)
                {
                    lvSimplex.Columns.Add("s" + (i + 1));
                }
                for (int i = 0; i < ConstraintInputForm.artNum; i += 1)
                {
                    lvSimplex.Columns.Add("a" + (i + 1));
                }
                lvSimplex.Columns.Add("Nilai");
                lvSimplex.Columns.Add("Rasio");

                ListViewItem item;
                ListViewItem data = ConstraintInputForm.dataZ.Items[0];
                
                item = new ListViewItem("Z");
                item.SubItems.Add("1");
                for (int j = 0; j < ProblemInputForm.varNum; j += 1)
                {
                    double x = Convert.ToDouble(data.SubItems[j + 1].Text);
                    if (x > 0)
                        item.SubItems.Add("-" + x);
                    else
                    {
                        x = x * -1;
                        item.SubItems.Add(x + "");
                    }
                }
                for (int j = 0; j < ConstraintInputForm.slackNum; j += 1)
                {
                    item.SubItems.Add("0");
                }
                for (int j = 0; j < ConstraintInputForm.artNum; j += 1)
                {
                    if(ProblemInputForm.kind=="Maximize")
                        item.SubItems.Add(""+M);
                    else if(ProblemInputForm.kind == "Minimize")
                        item.SubItems.Add("-" + M);
                }
                item.SubItems.Add("0");
                item.SubItems.Add("");
                lvSimplex.Items.Add(item);
                int sln = 0, art = 0;
                for (int i = 0; i < ProblemInputForm.conNum; i += 1)
                {
                    item = new ListViewItem("s" + (i + 1));
                    item.SubItems.Add("0");
                    data = ConstraintInputForm.dataC.Items[i];
                    for (int j = 0; j < ProblemInputForm.varNum; j += 1)
                    {
                        item.SubItems.Add(data.SubItems[j + 1].Text);
                    }
                    bool done = false;
                    for (int j = 0; j < ConstraintInputForm.slackNum; j += 1)
                    {
                        if (data.SubItems[ProblemInputForm.varNum+1].Text=="<=" && !done && j==sln)
                        {
                            item.SubItems.Add("1");
                            done = true;
                            sln += 1;
                        }else if (data.SubItems[ProblemInputForm.varNum+1].Text == ">=" && !done && j == sln)
                        {
                            item.SubItems.Add("-1");
                            done = true;
                            sln += 1;
                        }
                        else
                        {
                            item.SubItems.Add("0");
                        }
                    }
                    done = false;
                    for (int j = 0; j < ConstraintInputForm.artNum; j += 1)
                    {
                        if ((data.SubItems[ProblemInputForm.varNum + 1].Text == "=" || data.SubItems[ProblemInputForm.varNum + 1].Text == ">=") && !done && j == art)
                        {
                            done = true;
                            item.SubItems.Add("1");
                            art += 1;
                        }
                        else
                        {
                            item.SubItems.Add("0");
                        }
                    }
                    item.SubItems.Add(data.SubItems[ProblemInputForm.varNum + 2]);
                    item.SubItems.Add("");
                    lvSimplex.Items.Add(item);
                }
                
            }
        }

        private void initializeM()
        {
            M_IS_OK = true;
            //int tempCol;
            for (int i = ProblemInputForm.varNum + ConstraintInputForm.slackNum+1; 
                i< ProblemInputForm.varNum + ConstraintInputForm.slackNum + ConstraintInputForm.artNum+1;
                i += 1)
            {
                double point = Convert.ToDouble(lvSimplex.Items[0].SubItems[i+1].Text);
                if (ProblemInputForm.kind == "Maximize")
                {
                    if (point == M)
                    {
                        M_IS_OK = false;
                        for(int j = 1; j<ProblemInputForm.conNum+1; j += 1)
                        {
                            if (lvSimplex.Items[j].SubItems[i+1].Text == "1")
                            {
                                //tempCol = j;
                                for(int k=1; k< ProblemInputForm.varNum + ConstraintInputForm.slackNum + ConstraintInputForm.artNum+2; k += 1)
                                {
                                    double num = Convert.ToDouble(lvSimplex.Items[0].SubItems[k+1].Text);
                                    double num2 = Convert.ToDouble(lvSimplex.Items[j].SubItems[k+1].Text);
                                        lvSimplex.Items[0].SubItems[k+1].Text = ((-1*num2*M)+num)+"";
                                }
                            }

                        }
                    }
                }else if (ProblemInputForm.kind == "Minimize")
                {
                    if (point == -M)
                    {
                        M_IS_OK = false;
                        for (int j = 1; j < ProblemInputForm.conNum + 1; j += 1)
                        {
                            if (lvSimplex.Items[j].SubItems[i + 1].Text == "1")
                            {
                                //tempCol = j;
                                for (int k = 1; k < ProblemInputForm.varNum + ConstraintInputForm.slackNum + ConstraintInputForm.artNum + 2; k += 1)
                                {
                                    double num = Convert.ToDouble(lvSimplex.Items[0].SubItems[k + 1].Text);
                                    double num2 = Convert.ToDouble(lvSimplex.Items[j].SubItems[k + 1].Text);

                                    lvSimplex.Items[0].SubItems[k + 1].Text = ((num2 * M) + num) + "";
                                }
                            }

                        }
                    }
                }

            }
        }

        private void SimplexProgressForm_Load(object sender, EventArgs e)
        {
            if (Form1.method == "basic")
            {
                lbMethod.Text = "Simplex Basic";
            }
            else if (Form1.method == "big-M")
            {
                lbMethod.Text = "Simplex Penalty";
            }

            if (ProblemInputForm.kind == "Maximize")
            {
                lbMethod.Text += " > Maximize";
            }
            else if (ProblemInputForm.kind == "Minimize")
            {
                lbMethod.Text += " > Minimize";
            }
        }

        private void btBack_Click(object sender, EventArgs e)
        {
            if (phase > 0)
            {
                if (button2.Text=="Lanjut >>")
                {
                    removeHighLightColumn();
                    removeHighLightRow();
                }else
                {
                    button2.Text = "Lanjut >>";
                    removeHighLightZ();
                    IS_COMPLETE = false;
                }

                lvSimplex.Items.Clear();
                lvSimplex.Items.AddRange((from ListViewItem item in iterationData[phase - 1].Items
                                          select (ListViewItem)item.Clone()
                      ).ToArray());
                iterationData.RemoveAt(phase - 1);
                phase -= 1;
                lbIterasi.Text = "" + iterasi;
                if (iterasi > 0)
                {
                    iterasi -= 1;
                    chooseRow();
                    chooseColumn();
                }else
                {
                    M_IS_OK = false;
                }

            }
        }

        private void SimplexProgressForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();

        }
    }
}
